Own NAS
=======

Project of NAS device consisting of Odroid, Owncloud and mdadm.

#Setup steps

* Install mdadm
* Format drives
* Create mdadm md0 device from prepared drives
** sudo mdadm --create --verbose /dev/md0 --level=linear --raid-devices=2 /dev/sda1 /dev/sdb1
** sudo mkfs.ext4 /dev/md0
** sudo mkdir -p /mnt/md0
** sudo mount /dev/md0 /mnt/md0
** mdadm --detail --scan >> /etc/mdadm/mdadm.conf
* fstab entry - /dev/md0 /mnt/md0 ext4 defaults,nofail,discard 0 0
* auto start docker composer
** sudo cp systemd-script/own-nas.service /lib/systemd/system/.
** sudo systemctl daemon-reload
** sudo systemctl enable own-nas
* change boot to no-gui: sudo systemctl set-target multi-user.target

